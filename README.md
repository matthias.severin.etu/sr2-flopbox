# SR2-flopbox

## Remarques

La video montre le bon fonctionnement du projet et toutes les commandes sont affichées, dans l'ordre dans le fichier commandes_video.txt
Toutes les éxigences du TP ont été remplies, sauf le download et upload de repertoire entier.
J'ai quand même essayé de le faire mais j'ai uniquement réussi a renvoyer le contenu de tout les fichiers d'un repertoire du serveur FTP via un MultiPartMediaTypes.MULTIPART_MIXED, mais je n'ai pas trouvé la commande curl pour tous les télécharger.
En lancant le programme via le fichier .jar j'ai une erreur que je n'ai pas en lancant avec "mvn exec:java" : les commandes curl produisent une erreur "415 Unsupported Media Type", il faut donc lancer le programme via mvn exec:java

## Compilation

mvn clean compile

## Generation de la javadoc

mvn javadoc:javadoc

## Generation du fichier jar

mvn package

## Lancement du flopbox

mvn exec:java

## Exemples d'utilisation avec les commandes curl

- Ajouter le serveur webtp.fil.univ-lille1.fr au nom webtp :
curl -X POST -d 'adress=webtp.fil.univ-lille1.fr' http://localhost:8080/flopbox/ftp/webtp

- Lister le contenu d'un répertoire (ici la racine /) :
curl -H "username:XXX" -H "password:XXX" http://localhost:8080/flopbox/ftp/webtp/

- Stocker le fichier file (local) sur le serveur FTP a l'endroit /public_html/file :
curl -X POST -F file=@file -H "username:XXX" -H "password:XXX" http://localhost:8080/flopbox/ftp/webtp/file/public_html/file

- Récupérer tous les fichiers dans le dossier /public_html :
curl -H "username:XXX" -H "password:XXX" http://localhost:8080/flopbox/ftp/web/directory/public_html

- Récupérer le fichier /public_html/file et le stocker dans le fichier out :
curl -H "username:XXX" -H "password:XXX" http://localhost:8080/flopbox/ftp/web/file/public_html/file -o out

- Renommer le fichier /public_html/file en /public_html/newfile :
curl -X PUT -H "username:XXX" -H "password:XXX" -d "newName=/public_html/newfile" http://localhost:8080/flopbox/ftp/web/file/public_html/file

- Supprimer le fichier file :
curl -X DELETE -H "username:XXX" -H "password:XXX" http://localhost:8080/flopbox/ftp/web/file/public_html/file

- Créer le dossier /public_html/test :
curl -X POST -H "username:XXX" -H "password:XXX" http://localhost:8080/flopbox/ftp/web/directory/public_html/test

- Renommer le dossier /public_html en /public_html2 :
curl -X PUT -H "username:XXX" -H "password:XXX" -d "newName=/public_html2" http://localhost:8080/flopbox/ftp/web/directory/public_html

- Supprimer le dossier public_html/test :
curl -X DELETE -H "username:XXX" -H "password:XXX" http://localhost:8080/flopbox/ftp/web/directory/public_html/test

## Informations complémentaires

On peut paramétrer le port en ajoutant dans les commandes curl -H "port:1234".
L'utilisateur et le mot de passe peuvent ne pas être indiqué, la connexion au serveur FTP se fera alors en mode anonyme.
