package fil.sr2.flopbox;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

public class ClientFTP {

	private FTPClient ftpClient;

	/**
	 * Create the new FTPClient
	 */
	public ClientFTP() {
		ftpClient = new FTPClient();
	}

	/**
	 * Store the file on the FTP server
	 * 
	 * @param file the content of the file
	 * @param path the path of the file
	 * @throws FTPException
	 */
	public void storeFile(InputStream file, String path) throws FTPException {
		try {
			ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
			ftpClient.enterLocalPassiveMode();
			ftpClient.storeFile(path, file);
		} catch (IOException e) {
			throw new FTPException("Failled to store the file\n");
		}
	}

	/**
	 * Give the file at the given path
	 * 
	 * @param path the path of file
	 * @return the file
	 * @throws FTPException
	 */
	public File getFile(String path) throws FTPException {
		File file = new File("/tmp/" + path);
		ftpClient.enterLocalPassiveMode();
		try {
			ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			OutputStream os = new FileOutputStream(file);
			ftpClient.retrieveFile(path, os);
			return file;
		} catch (IOException e) {
			file.getParentFile().mkdir();
			throw new FTPException("Failled to retreive the file\n");
		}
	}

	/**
	 * List all informations of files and directories contains at this path
	 * 
	 * @param path the path
	 * @return the list of all informations
	 * @throws FTPException
	 */
	public String list(String path) throws FTPException {
		String res = "";
		ftpClient.enterLocalPassiveMode();
		try {
			FTPFile[] files = ftpClient.listFiles(path);
			for (FTPFile file : files) {
				res += file.getRawListing() + "\n";
			}
		} catch (IOException e) {
			throw new FTPException("Cannot list the directory\n");
		}
		return res;
	}

	/**
	 * Connect to the FTP server (in anonymous if username or password are not
	 * given)
	 * 
	 * @param adress   adress of the FTP server
	 * @param username the username
	 * @param password the password
	 * @param port     the port (21 if not define)
	 * @throws FTPException
	 */
	public void connect(String adress, String username, String password, int port) throws FTPException {
		if (username == null || password == null) {
			username = password = "anonymous";
		}
		if (port == 0) {
			port = 21;
		}
		try {
			ftpClient.connect(adress, port);
			ftpClient.login(username, password);
		} catch (IOException e) {
			throw new FTPException("Enable to connect\n");
		}
	}

	/**
	 * Disconnect the client
	 */
	public void disconnect() {
		if (ftpClient.isConnected()) {
			try {
				ftpClient.disconnect();
			} catch (IOException e) {
				// do nothing
			}
		}
	}

	/**
	 * Rename the file
	 * 
	 * @param path    the path of the file
	 * @param newName the new name of the file
	 * @return true if succesfull, else false
	 */
	public boolean rename(String path, String newName) {
		try {
			return ftpClient.rename(path, newName);
		} catch (IOException e) {
			return false;
		}
	}

	/**
	 * delete the file
	 * 
	 * @param path the path of the file
	 * @return true if succesfull, else false
	 */
	public boolean deleteFile(String path) {
		try {
			return ftpClient.deleteFile(path);
		} catch (IOException e) {
			return false;
		}
	}

	/**
	 * delete the directory
	 * 
	 * @param path the path of the directory
	 * @return true if succesfull, else false
	 */
	public boolean deleteDirectory(String path) {
		ftpClient.enterLocalPassiveMode();
		try {
			FTPFile[] files = ftpClient.listFiles(path);
			for (FTPFile file : files) {
				if (file.isDirectory()) {
					deleteDirectory(path + "/" + file.getName());
				} else {
					ftpClient.deleteFile(path + "/" + file.getName());
				}
			}
			return ftpClient.removeDirectory(path);
		} catch (IOException e) {
			return false;
		}
	}

	/**
	 * create a directory
	 * 
	 * @param path the path of the directory
	 * @return true if succesfull, else false
	 */
	public boolean createDirectory(String path) {
		try {
			return ftpClient.makeDirectory(path);
		} catch (IOException e) {
			return false;
		}
	}

	/**
	 * Get all files from the directory recursively
	 * 
	 * @param path the path of the directory
	 * @return the list of files
	 * @throws FTPException
	 */
	public ArrayList<File> getFilesFromDirectory(String path) throws FTPException {
		ArrayList<File> res = new ArrayList<>();
		ftpClient.enterLocalPassiveMode();
		try {
			FTPFile[] files = ftpClient.listFiles(path);
			for (FTPFile file : files) {
				if (file.isDirectory()) {
					res.addAll(getFilesFromDirectory(path + "/" + file.getName()));
				} else {
					res.add(getFile(path + "/" + file.getName()));
				}
			}
		} catch (IOException e) {
			throw new FTPException("Failled to get files from the directory\n");
		}
		return res;
	}
}
