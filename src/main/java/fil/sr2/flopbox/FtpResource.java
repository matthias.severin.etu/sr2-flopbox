package fil.sr2.flopbox;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartMediaTypes;

/**
 * Root resource (exposed at "ftp" path)
 */
@Path("/ftp")
public class FtpResource {

	private static Map<String, String> ftpMap = new HashMap<>();

	/**
	 * Create the link between the FTP server and this name
	 * 
	 * @param name   the name of the FTP server
	 * @param adress the FTP server
	 * @return the response of this command
	 */
	@POST
	@Path("/{name}")
	public Response newFTP(@PathParam("name") String name, @FormParam("adress") String adress) {
		ftpMap.put(name, adress);
		return Response.status(Response.Status.OK).entity("Link to the FTP server succesfully added.\n").build();
	}

	/**
	 * Update the FTP server linked to this name
	 * 
	 * @param name   the name of the FTP server
	 * @param adress the new FTP server
	 * @return the response of this command
	 */
	@PUT
	@Path("/{name}")
	public Response updateFTP(@PathParam("name") String name, @FormParam("adress") String adress) {
		ftpMap.replace(name, adress);
		return Response.status(Response.Status.OK).entity("Link to the FTP server succesfully updated.\n").build();
	}

	/**
	 * Remove the FTP server linked to this name
	 * 
	 * @param name the name of the FTP server
	 * @return the response of this command
	 */
	@DELETE
	@Path("/{name}")
	public Response removeFTP(@PathParam("name") String name) {
		ftpMap.remove(name);
		return Response.status(Response.Status.OK).entity("Link to the FTP server succesfully deleted.\n").build();
	}

	/**
	 * Show the FTP server linked to this name
	 * 
	 * @param name the name of the FTP server
	 * @return the response of this command
	 */
	@GET
	@Path("/{name}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getFTP(@PathParam("name") String name) {
		return Response.status(Response.Status.OK).entity("adress FTP: " + ftpMap.get(name) + "\n").build();
	}

	/**
	 * list all informations of files and directories contains in the selected
	 * directory
	 * 
	 * @param name     the name of the FTP server
	 * @param path     the path of the selected directory
	 * @param username username for the FTP server
	 * @param password password for the FTP server
	 * @param port     port of the FTP server
	 * @return the response of this command
	 */
	@GET
	@Path("/{name}/{path: .*}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response listFTP(@PathParam("name") String name, @PathParam("path") String path,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port) {
		ResponseBuilder res;
		try {
			ClientFTP ftp = new ClientFTP();
			ftp.connect(ftpMap.get(name), username, password, port);
			res = Response.status(Response.Status.OK).entity(ftp.list(path));
			ftp.disconnect();
		} catch (FTPException e) {
			res = Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage());
		}
		return res.build();
	}

	/**
	 * return the file
	 * 
	 * @param name     the name of the FTP server
	 * @param path     the path of the file to return
	 * @param username username for the FTP server
	 * @param password password for the FTP server
	 * @param port     port of the FTP server
	 * @return the response of this command
	 */
	@GET
	@Path("/{name}/file/{path: .*}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getFileFTP(@PathParam("name") String name, @PathParam("path") String path,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port) {
		ResponseBuilder res;
		try {
			ClientFTP ftp = new ClientFTP();
			ftp.connect(ftpMap.get(name), username, password, port);
			res = Response.status(Response.Status.OK).entity(ftp.getFile(path));
			ftp.disconnect();
		} catch (FTPException e) {
			res = Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage());
		}
		return res.build();
	}

	/**
	 * List the content of all file on the directory
	 * 
	 * @param name     the name of the FTP server
	 * @param path     the path of the directory to list
	 * @param username username for the FTP server
	 * @param password password for the FTP server
	 * @param port     port of the FTP server
	 * @return the response of this command
	 */
	@GET
	@Path("/{name}/directory/{path: .*}")
	@Produces(MultiPartMediaTypes.MULTIPART_MIXED)
	public Response getDirectoryFTP(@PathParam("name") String name, @PathParam("path") String path,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port) {
		ResponseBuilder res;
		try {
			ClientFTP ftp = new ClientFTP();
			ftp.connect(ftpMap.get(name), username, password, port);
			ArrayList<File> files = ftp.getFilesFromDirectory(path);
			MultiPart mp = new MultiPart();
			for (File file : files) {
				mp.bodyPart(file, MediaType.APPLICATION_OCTET_STREAM_TYPE);
			}
			mp.close();
			res = Response.status(Response.Status.OK).entity(mp);
			ftp.disconnect();
		} catch (FTPException e) {
			res = Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage());
		} catch (IOException e) {
			res = Response.status(Response.Status.BAD_REQUEST).entity("Enable to get the directory");
		}
		return res.build();
	}

	/**
	 * Upload a file
	 * 
	 * @param uploadedInputStream the InputStream of the file
	 * @param fileDetail          details of the file
	 * @param name                the name of the FTP server
	 * @param path                the path of the file to create
	 * @param username            username for the FTP server
	 * @param password            password for the FTP server
	 * @param port                port of the FTP server
	 * @return the response of this command
	 */
	@POST
	@Path("/{name}/file/{path: .*}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFileFTP(@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail, @PathParam("name") String name,
			@PathParam("path") String path, @HeaderParam("username") String username,
			@HeaderParam("password") String password, @HeaderParam("port") int port) {
		ResponseBuilder res;
		try {
			ClientFTP ftp = new ClientFTP();
			ftp.connect(ftpMap.get(name), username, password, port);
			ftp.storeFile(uploadedInputStream, path);
			ftp.disconnect();
			res = Response.status(Response.Status.OK).entity("File succesfully uploaded\n");
		} catch (FTPException e) {
			res = Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage());
		}
		return res.build();
	}

	/**
	 * Create a directory
	 * 
	 * @param name     the name of the FTP server
	 * @param path     the path of the directory to create
	 * @param username username for the FTP server
	 * @param password password for the FTP server
	 * @param port     port of the FTP server
	 * @return the response of this command
	 */
	@POST
	@Path("/{name}/directory/{path: .*}")
	public Response createDirectoryFTP(@PathParam("name") String name, @PathParam("path") String path,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port) {
		ResponseBuilder res;
		try {
			ClientFTP ftp = new ClientFTP();
			ftp.connect(ftpMap.get(name), username, password, port);
			if (ftp.createDirectory(path)) {
				res = Response.status(Response.Status.OK).entity("Directory succesfully created\n");
			} else {
				res = Response.status(Response.Status.BAD_REQUEST).entity("Failled to create the directory\n");
			}
			ftp.disconnect();
		} catch (FTPException e) {
			res = Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage());
		}
		return res.build();
	}

	/**
	 * Rename the file
	 * 
	 * @param name     the name of the FTP server
	 * @param path     the path of the file to rename
	 * @param username username for the FTP server
	 * @param password password for the FTP server
	 * @param port     port of the FTP server
	 * @param newName  the new name of the file
	 * @return the response of this command
	 */
	@PUT
	@Path("/{name}/file/{path: .*}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response renameFileFTP(@PathParam("name") String name, @PathParam("path") String path,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port, @FormParam("newName") String newName) {
		ResponseBuilder res;
		try {
			ClientFTP ftp = new ClientFTP();
			ftp.connect(ftpMap.get(name), username, password, port);
			if (ftp.rename(path, newName)) {
				res = Response.status(Response.Status.OK).entity("File succesfully renamed\n");
			} else {
				res = Response.status(Response.Status.BAD_REQUEST).entity("Failled to rename the file\n");
			}
			ftp.disconnect();
		} catch (FTPException e) {
			res = Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage());
		}
		return res.build();
	}

	/**
	 * Delete the file
	 * 
	 * @param name     the name of the FTP server
	 * @param path     the path of the file to delete
	 * @param username username for the FTP server
	 * @param password password for the FTP server
	 * @param port     port of the FTP server
	 * @return the response of this command
	 */
	@DELETE
	@Path("/{name}/file/{path: .*}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteFileFTP(@PathParam("name") String name, @PathParam("path") String path,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port) {
		ResponseBuilder res;
		try {
			ClientFTP ftp = new ClientFTP();
			ftp.connect(ftpMap.get(name), username, password, port);
			if (ftp.deleteFile(path)) {
				res = Response.status(Response.Status.OK).entity("File succesfully deleted\n");
			} else {
				res = Response.status(Response.Status.BAD_REQUEST).entity("Failled to delete the file\n");
			}
			ftp.disconnect();
		} catch (FTPException e) {
			res = Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage());
		}
		return res.build();
	}

	/**
	 * Rename the directory
	 * 
	 * @param name     the name of the FTP server
	 * @param path     the path of the directory to rename
	 * @param username username for the FTP server
	 * @param password password for the FTP server
	 * @param port     port of the FTP server
	 * @param newName  the new name of the directory
	 * @return the response of this command
	 */
	@PUT
	@Path("/{name}/directory/{path: .*}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response renameDirectoryFTP(@PathParam("name") String name, @PathParam("path") String path,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port, @FormParam("newName") String newName) {
		ResponseBuilder res;
		try {
			ClientFTP ftp = new ClientFTP();
			ftp.connect(ftpMap.get(name), username, password, port);
			if (ftp.rename(path, newName)) {
				res = Response.status(Response.Status.OK).entity("Directory succesfully renamed\n");
			} else {
				res = Response.status(Response.Status.BAD_REQUEST).entity("Failled to rename the Directory\n");
			}
			ftp.disconnect();
		} catch (FTPException e) {
			res = Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage());
		}
		return res.build();
	}

	/**
	 * Delete the directory
	 * 
	 * @param name     the name of the FTP server
	 * @param path     the path of the directory to delete
	 * @param username username for the FTP server
	 * @param password password for the FTP server
	 * @param port     port of the FTP server
	 * @return the response of this command
	 */
	@DELETE
	@Path("/{name}/directory/{path: .*}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteDirectoryFTP(@PathParam("name") String name, @PathParam("path") String path,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port) {
		ResponseBuilder res;
		try {
			ClientFTP ftp = new ClientFTP();
			ftp.connect(ftpMap.get(name), username, password, port);
			if (ftp.deleteDirectory(path)) {
				res = Response.status(Response.Status.OK).entity("Directory succesfully deleted\n");
			} else {
				res = Response.status(Response.Status.BAD_REQUEST).entity("Failled to delete the directory\n");
			}
			ftp.disconnect();
		} catch (FTPException e) {
			res = Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage());
		}
		return res.build();
	}
}
