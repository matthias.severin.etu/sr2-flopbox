package fil.sr2.flopbox;

public class FTPException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public FTPException(String msg) {
		super(msg);
	}

}
